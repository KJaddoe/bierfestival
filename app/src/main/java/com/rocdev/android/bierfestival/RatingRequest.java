package com.rocdev.android.bierfestival;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Sybren on 29-6-2016.
 */
public class RatingRequest extends StringRequest {

    private static final String RATING_REQUEST_URL = "http://app.flevobierfestival.nl/setRating.php";
    private Map<String, String> params;

    public RatingRequest(String id, String bier, String bierID, String email, int rating, Response.Listener<String> listener){
        super(Request.Method.POST, RATING_REQUEST_URL , listener, null);

        params = new HashMap<>();
        params.put("id", id);
        params.put("bier", bier);
        params.put("bierID", bierID);
        params.put("email", email);
        params.put("rating", rating + "");

    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }
}
