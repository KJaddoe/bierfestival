package com.rocdev.android.bierfestival;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class TypeActivity extends AppCompatActivity {
    TextView titel, downloadView;
    String naamType, email;
    ListView bierLijst;

    String URL_Bieren = "http://app.flevobierfestival.nl/rest/bieren/";
    ArrayList<String> bieren_title_list, bieren_URL_list, bierID;
    ArrayAdapter<String> bierenAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_type);

        titel = (TextView) findViewById(R.id.titel);
        Intent intent = getIntent();
        naamType = intent.getStringExtra("type");
        email = intent.getStringExtra("email");

        downloadView = (TextView) findViewById(R.id.downloadView);
        bierLijst = (ListView) findViewById(R.id.bierLijst);
        bieren_URL_list = new ArrayList<>();
        bieren_title_list = new ArrayList<>();
        bierID = new ArrayList<>();

        bierenAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, bieren_title_list);
        bierLijst.setAdapter(bierenAdapter);
        bierLijst.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Intent intent = new Intent(getApplicationContext(), BierActivity.class);
                intent.putExtra("bierID", bierID.get(position));
                intent.putExtra("email", email);
                startActivity(intent);
            }
        });


        downloadView.setVisibility(View.VISIBLE);
        new BierDownloadTask().execute();


    }

    class BierDownloadTask extends AsyncTask<Void, String, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
        String resArray = "";
        HttpURLConnection connection = null;
        InputStream in = null;
        InputStreamReader reader = null;

        try {
            URL urlIDS = new URL(URL_Bieren);
            connection = (HttpURLConnection) urlIDS.openConnection();
            in = connection.getInputStream();
            reader = new InputStreamReader(in);
            int data = reader.read();
            while (data != -1){
                char c = (char) data;
                resArray += c;
                data = reader.read();
            }
            Log.i("types->bieren", resArray);
            JSONArray jsonArray = new JSONArray(resArray);
            int index = 0;
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject;
                jsonObject = jsonArray.getJSONObject(i);
                if (jsonObject.has("title")){
                    if (jsonObject.getString("field_type").equals(naamType)) {
                        bierID.add(jsonObject.getString("nid"));
                        bieren_title_list.add(jsonObject.getString("title"));
                        bieren_URL_list.add("http://app.flevobierfestival.nl/node/"+jsonObject.getString("nid"));
                        publishProgress("Downloaded: " + index);
                        index++;
                    }
                }
            }

        } catch (Exception e){
            e.printStackTrace();
        } finally {
            if (connection != null){
                connection.disconnect();
            }
            if (in != null){
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (reader != null){
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            String update = values[0];
            downloadView.setText(update);
            downloadView.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            bierenAdapter.notifyDataSetChanged();
            downloadView.setVisibility(View.GONE);
            titel.setText("Alle bieren van het type: "+naamType);
        }
    }
}
