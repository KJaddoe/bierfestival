package com.rocdev.android.bierfestival;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class BierActivity extends AppCompatActivity {
    String bier;
    String url = "http://app.flevobierfestival.nl/rest/bier/";
    String ratingUrl = "http://app.flevobierfestival.nl/getAvgRating.php";
    String ratingIDUrl = "http://app.flevobierfestival.nl/getAllRatings.php";
    String naam, beschrijving, imageUrl, type, brouwer, percentage, bestelURL;
    TextView downloadView;
    TextView titleView;
    TextView beschrijvingView;
    TextView typeView;
    TextView brouwerView;
    TextView percentageView;
    TextView beschrijvingTV;
    ImageView imageView;
    Button bestelButton;
    Button eigenRateButton;
    String email;
    RatingBar userRatingBar, bierRatingBar;
    LinearLayout ratingLayout;
    float gemrating, userRating;
    String ratingID = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bier);

        downloadView = (TextView) findViewById(R.id.downloadView);
        imageView = (ImageView) findViewById(R.id.imageView);
        bestelButton = (Button) findViewById(R.id.bestelButton);
        eigenRateButton = (Button) findViewById(R.id.eigenRateButton);
        userRatingBar = (RatingBar) findViewById(R.id.eigenRatingBar);
        bierRatingBar = (RatingBar) findViewById(R.id.bierRatingBar);
        ratingLayout = (LinearLayout) findViewById(R.id.ratingLayout);
        gemrating = 0;

        Intent intent = getIntent();
        bier = intent.getStringExtra("bierID");
        email = intent.getStringExtra("email");


        titleView = (TextView) findViewById(R.id.titel);
        beschrijvingView = (TextView) findViewById(R.id.beschrijving);
        typeView = (TextView) findViewById(R.id.type);
        brouwerView = (TextView) findViewById(R.id.brouwer);
        percentageView = (TextView) findViewById(R.id.percentage);
        beschrijvingTV = (TextView) findViewById(R.id.textView4);

        downloadView.setVisibility(View.VISIBLE);

        new BierDownloadTask().execute();
        new DownloadGemRating().execute();
        new DownloadUserRating().execute();

        eigenRateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final int rating = (int) userRatingBar.getRating();


                // Response received from the server
                Response.Listener<String> responseListener = new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            boolean success = jsonResponse.getBoolean("success");

                            if (success) {
                                finish();

                                Toast.makeText(BierActivity.this, "Uw rating is succesvol verstuurd!", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(BierActivity.this, BierActivity.class);
                                intent.putExtra("email", email);
                                intent.putExtra("bierID", bier);



                                BierActivity.this.startActivity(intent);
                            } else {
                                AlertDialog.Builder builder = new AlertDialog.Builder(BierActivity.this);
                                builder.setMessage("Rating mislukt")
                                        .setNegativeButton("Probeer opnieuw", null)
                                        .create()
                                        .show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                };

                RatingRequest ratingRequest = new RatingRequest(ratingID, naam, bier, email, rating, responseListener);
                RequestQueue queue = Volley.newRequestQueue(BierActivity.this);
                queue.add(ratingRequest);
            }
        });

    }




    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView imageView;

        public DownloadImageTask(ImageView imageView) {
            this.imageView = imageView;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {imageView.setImageBitmap(result);}
    }

    class BierDownloadTask extends AsyncTask<Void, String, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            String resArray = "";
            HttpURLConnection connection = null;
            InputStream in = null;
            InputStreamReader reader = null;

            try {
                URL urlIDS = new URL(url + bier);
                connection = (HttpURLConnection) urlIDS.openConnection();
                in = connection.getInputStream();
                reader = new InputStreamReader(in);
                int data = reader.read();
                while (data != -1){
                    char c = (char) data;
                    resArray += c;
                    data = reader.read();
                }
                Log.i("Bier", resArray);
                JSONArray jsonArray = new JSONArray(resArray);
                int index = 0;
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject;
                    jsonObject = jsonArray.getJSONObject(i);
                    if (jsonObject.has("title")){
                        imageUrl = jsonObject.getString("field_image");
                        naam = jsonObject.getString("title");
                        beschrijving = jsonObject.getString("body");
                        type = jsonObject.getString("field_type");
                        brouwer = jsonObject.getString("field_brouwer");
                        percentage = jsonObject.getString("field_alcohol_percentage");
                        bestelURL = jsonObject.getString("field_bestel_link");
                        publishProgress("Downloaded: " + index);
                        index++;
                    }
                }

            } catch (Exception e){
                e.printStackTrace();
            } finally {
                if (connection != null){
                    connection.disconnect();
                }
                if (in != null){
                    try {
                        in.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (reader != null){
                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            String update = values[0];
            downloadView.setText(update);
            downloadView.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            new DownloadImageTask((ImageView) findViewById(R.id.imageView))
                    .execute(imageUrl);
            downloadView.setVisibility(View.GONE);
            ratingLayout.setVisibility(View.VISIBLE);
            titleView.setText("" + naam);
            beschrijvingView.setText("" + beschrijving);
            typeView.setText("Type: " + type);
            brouwerView.setText("Brouwer: " + brouwer);
            percentageView.setText("Percentage: " + percentage);
            beschrijvingTV.setText("Beschrijving");

            if (bestelURL != null && !bestelURL.isEmpty()){
                bestelButton.setVisibility(View.VISIBLE);
                bestelButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(bestelURL));
                        startActivity(browserIntent);
                    }
                });
            } else {
                bestelButton.setVisibility(View.GONE);
            }
        }
    }

    class DownloadGemRating extends AsyncTask<Void, String, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            String resArray = "";
            HttpURLConnection connection = null;
            InputStream in = null;
            InputStreamReader reader = null;

            try {
                URL urlIDS = new URL(ratingUrl);
                connection = (HttpURLConnection) urlIDS.openConnection();
                in = connection.getInputStream();
                reader = new InputStreamReader(in);
                int data = reader.read();
                while (data != -1){
                    char c = (char) data;
                    resArray += c;
                    data = reader.read();
                }
                Log.i("BierRating", resArray);
                JSONArray jsonArray = new JSONArray(resArray);
                int index = 0;
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject;
                    jsonObject = jsonArray.getJSONObject(i);
                    if (jsonObject.has("gemrating")){
                        if (jsonObject.getString("bierID").equals(bier)) {
                            gemrating = Float.parseFloat(jsonObject.getString("gemrating"));
                        }
                        publishProgress("Downloaded: " + index);
                        index++;
                    }
                }

            } catch (Exception e){
                e.printStackTrace();
            } finally {
                if (connection != null){
                    connection.disconnect();
                }
                if (in != null){
                    try {
                        in.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (reader != null){
                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            String update = values[0];
            downloadView.setText(update);
            downloadView.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            new DownloadImageTask((ImageView) findViewById(R.id.imageView))
                    .execute(imageUrl);
            bierRatingBar.setRating(gemrating);
            downloadView.setVisibility(View.GONE);
            ratingLayout.setVisibility(View.VISIBLE);

        }
    }

    class DownloadUserRating extends AsyncTask<Void, String, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            String resArray = "";
            HttpURLConnection connection = null;
            InputStream in = null;
            InputStreamReader reader = null;

            try {
                URL urlIDS = new URL(ratingIDUrl);
                connection = (HttpURLConnection) urlIDS.openConnection();
                in = connection.getInputStream();
                reader = new InputStreamReader(in);
                int data = reader.read();
                while (data != -1){
                    char c = (char) data;
                    resArray += c;
                    data = reader.read();
                }
                Log.i("BierRatingID", resArray);
                JSONArray jsonArray = new JSONArray(resArray);
                int index = 0;
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject;
                    jsonObject = jsonArray.getJSONObject(i);
                    if (jsonObject.has("id")){
                        if (jsonObject.getString("bierID").equals(bier) && jsonObject.getString("email").equals(email)) {
                            ratingID = jsonObject.getString("id");
                            userRating = Float.parseFloat(jsonObject.getString("rating"));
                        }
                        publishProgress("Downloaded: " + index);
                        index++;
                    }
                }

            } catch (Exception e){
                e.printStackTrace();
            } finally {
                if (connection != null){
                    connection.disconnect();
                }
                if (in != null){
                    try {
                        in.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (reader != null){
                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            String update = values[0];
            downloadView.setText(update);
            downloadView.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            new DownloadImageTask((ImageView) findViewById(R.id.imageView))
                    .execute(imageUrl);
            bierRatingBar.setRating(gemrating);
            userRatingBar.setRating(userRating);
            downloadView.setVisibility(View.GONE);
        }
    }
}
