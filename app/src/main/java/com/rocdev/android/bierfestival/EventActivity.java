package com.rocdev.android.bierfestival;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class EventActivity extends AppCompatActivity {
    String nid, email;
    String url = "http://app.flevobierfestival.nl/rest/evenement/";
    String naam, beschrijving, begintijd, eindtijd;
    TextView downloadView, titleView, beschrijvingView, begintijdView, eindtijdView, beschrijvingLabel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);

        downloadView = (TextView) findViewById(R.id.downloadView);

        Intent intent = getIntent();
        nid = intent.getStringExtra("eventID");
        email = intent.getStringExtra("email");

        titleView = (TextView) findViewById(R.id.titel);
        begintijdView = (TextView) findViewById(R.id.begintijd);
        eindtijdView = (TextView) findViewById(R.id.eindtijd);
        beschrijvingLabel = (TextView) findViewById(R.id.beschrijvingLabel);
        beschrijvingView = (TextView) findViewById(R.id.beschrijving);

        downloadView.setVisibility(View.VISIBLE);

        new EventDownloadTask().execute();
    }

    class EventDownloadTask extends AsyncTask<Void, String, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            String resArray = "";
            HttpURLConnection connection = null;
            InputStream in = null;
            InputStreamReader reader = null;

            try {
                URL urlIDS = new URL(url + nid);
                connection = (HttpURLConnection) urlIDS.openConnection();
                in = connection.getInputStream();
                reader = new InputStreamReader(in);
                int data = reader.read();
                while (data != -1){
                    char c = (char) data;
                    resArray += c;
                    data = reader.read();
                }
                Log.i("brouwer", resArray);
                JSONArray jsonArray = new JSONArray(resArray);
                int index = 0;
                for (int i = 0; i < jsonArray.length(); i++){
                    JSONObject jsonObject;
                    jsonObject = jsonArray.getJSONObject(i);
                    if (jsonObject.has("title")){
                        naam = jsonObject.getString("title");
                        beschrijving = jsonObject.getString("body");
                        begintijd = jsonObject.getString("field_begin_tijd");
                        eindtijd = jsonObject.getString("field_eind_tijd");
                        publishProgress("Downloaded: " + index);
                        index++;
                    }
                }

            } catch (Exception e){
                e.printStackTrace();
            } finally {
                if (connection != null){
                    connection.disconnect();
                }
                if (in != null){
                    try {
                        in.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (reader != null){
                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            String update = values[0];
            downloadView.setText(update);
            downloadView.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            downloadView.setVisibility(View.GONE);
            titleView.setText("" + naam);
            begintijdView.setText("van: " + begintijd);
            eindtijdView.setText("tot: " + eindtijd);
            beschrijvingLabel.setVisibility(View.VISIBLE);
            beschrijvingView.setText("" + beschrijving);
        }
    }
}
