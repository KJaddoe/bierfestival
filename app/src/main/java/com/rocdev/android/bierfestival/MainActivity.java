package com.rocdev.android.bierfestival;

import android.app.ProgressDialog;
import android.app.TabActivity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.HorizontalScrollView;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    /**
     * Created by:
     * Aleksandro Dokic         -> http://adokic.nl
     * Karan Jaddoe             -> http://karanjaddoe.nl
     * Sybren van der Schuur    -> /
     */

    final String URL_Festival = "http://app.flevobierfestival.nl/rest/festival/";
    final String URL_Events = "http://app.flevobierfestival.nl/rest/evenementen/";

    final String URL_Bieren = "http://app.flevobierfestival.nl/rest/bieren/";
    final String URL_Brouwers = "http://app.flevobierfestival.nl/rest/brouwers/";
    final String URL_Types = "http://app.flevobierfestival.nl/rest/types/";

    ArrayList<String> brouwerID, bierID, eventID;

    ListView evenementenListView, bierenListView, brouwersListView, typesListView;
    TextView evenementenDownloadView, bierenDownloadView, brouwersDownloadView;
    ArrayAdapter<String> evenementenAdapter, bierenAdapter, brouwersAdapter, typesAdapter;

    ArrayList<String> evenementen_title_list;
    ArrayList<String> festival_title_list;

    ArrayList<String> bieren_title_list;
    ArrayList<String> brouwers_title_list;
    ArrayList<String> types_title_list;

    SharedPreferences prefs;
    String email, userEmail;

    TabHost tabHost;

    String eventsID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        prefs = this.getPreferences(Context.MODE_PRIVATE);

        email = getIntent().getStringExtra("email");
        userEmail = prefs.getString("email", email);


        //Brouwers List
        brouwersListView = (ListView) findViewById(R.id.BrouwerListView);
        brouwersDownloadView = (TextView) findViewById(R.id.downloadView);
        brouwersDownloadView.setVisibility(View.VISIBLE);
        brouwers_title_list = new ArrayList<>();
        brouwerID = new ArrayList<>();

        brouwersAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, brouwers_title_list);
        brouwersListView.setAdapter(brouwersAdapter);
        brouwersListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Intent intent = new Intent(getApplicationContext(), BrouwerActivity.class);
                intent.putExtra("brouwerID", brouwerID.get(position));
                intent.putExtra("email", userEmail);
                startActivity(intent);
            }
        });


        //Bieren List
        bierenListView = (ListView) findViewById(R.id.BierenListView);
        bierenDownloadView = (TextView) findViewById(R.id.downloadView1);
        bierenDownloadView.setVisibility(View.VISIBLE);
        bieren_title_list = new ArrayList<>();
        bierID = new ArrayList<>();

        bierenAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, bieren_title_list);
        bierenListView.setAdapter(bierenAdapter);
        bierenListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Intent intent = new Intent(getApplicationContext(), BierActivity.class);
                intent.putExtra("bierID", bierID.get(position));
                intent.putExtra("email", userEmail);
                startActivity(intent);
            }
        });


        //Evenementen List

        evenementenListView = (ListView) findViewById(R.id.EvenementListView);
        evenementenDownloadView = (TextView) findViewById(R.id.downloadView2);
        evenementenDownloadView.setVisibility(View.VISIBLE);
        evenementen_title_list = new ArrayList<>();
        eventID= new ArrayList<>();

        evenementenAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, evenementen_title_list);
        evenementenListView.setAdapter(evenementenAdapter);
        evenementenListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Intent intent = new Intent(getApplicationContext(), EventActivity.class);
                intent.putExtra("eventID", eventID.get(position));
                intent.putExtra("email", userEmail);
                startActivity(intent);
            }
        });


        //Types List
        typesListView = (ListView) findViewById(R.id.typesListView);
        types_title_list = new ArrayList<>();

        typesAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, types_title_list);
        typesListView.setAdapter(typesAdapter);
        typesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Intent intent = new Intent(getApplicationContext(), TypeActivity.class);
                intent.putExtra("type", types_title_list.get(position));
                intent.putExtra("email", userEmail);
                startActivity(intent);
            }
        });

        if (isOnline()) {
            requestData();
        } else {
            Toast.makeText(this, "Controleer uw netwerk verbinding!", Toast.LENGTH_LONG).show();
        }

        tabHost = (TabHost)findViewById(R.id.tabHost);
        tabHost.setup();

        TabHost.TabSpec spec = tabHost.newTabSpec("Evenementen");
        spec.setContent(R.id.tabEvent);
        spec.setIndicator("Evenementen");
        tabHost.addTab(spec);

        spec = tabHost.newTabSpec("Brouwers");
        spec.setContent(R.id.tabBrouwers);
        spec.setIndicator("Brouwers");
        tabHost.addTab(spec);

        spec = tabHost.newTabSpec("Bieren");
        spec.setContent(R.id.tabBieren);
        spec.setIndicator("Bieren");
        tabHost.addTab(spec);

        spec = tabHost.newTabSpec("Type");
        spec.setContent(R.id.tabType);
        spec.setIndicator("Type");
        tabHost.addTab(spec);


        HorizontalScrollView horizontalScrollView = (HorizontalScrollView)
                findViewById(R.id.horizontalScrollView);

        horizontalScrollView.setHorizontalScrollBarEnabled(false);
    }

    private void requestData() {
        new BrouwersDownloadTask().execute();
        new BierenDownloadTask().execute();
        new FestivalDownloadTask().execute();
        new TypesDownloadTask().execute();
    }

    protected boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        } else {
            return false;
        }
    }


    class BrouwersDownloadTask extends AsyncTask<Void, String, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            String resArray = "";
            HttpURLConnection connection = null;
            InputStream in = null;
            InputStreamReader reader = null;

            try {
                URL urlIDS = new URL(URL_Brouwers);
                connection = (HttpURLConnection) urlIDS.openConnection();
                in = connection.getInputStream();
                reader = new InputStreamReader(in);
                int data = reader.read();
                while (data != -1){
                    char c = (char) data;
                    resArray += c;
                    data = reader.read();
                }
                Log.i("Brouwerlijst", resArray);
                JSONArray jsonArray = new JSONArray(resArray);
                int index = 0;
                for (int i = 0; i < jsonArray.length(); i++){
                    JSONObject jsonObject;
                    jsonObject = jsonArray.getJSONObject(i);
                    if (jsonObject.has("title")){
                        brouwerID.add(jsonObject.getString("nid"));
                        brouwers_title_list.add(jsonObject.getString("title"));
                        publishProgress("Downloaded: " + index);
                        index++;
                    }
                }

            } catch (Exception e){
                e.printStackTrace();
            } finally {
                if (connection != null){
                    connection.disconnect();
                }
                if (in != null){
                    try {
                        in.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (reader != null){
                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            String update = values[0];
            brouwersDownloadView.setText(update);
            brouwersDownloadView.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            brouwersAdapter.notifyDataSetChanged();
            brouwersDownloadView.setVisibility(View.GONE);
        }
    }

    class BierenDownloadTask extends AsyncTask<Void, String, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            String resArray = "";
            HttpURLConnection connection = null;
            InputStream in = null;
            InputStreamReader reader = null;

            try {
                URL urlIDS = new URL(URL_Bieren);
                connection = (HttpURLConnection) urlIDS.openConnection();
                in = connection.getInputStream();
                reader = new InputStreamReader(in);
                int data = reader.read();
                while (data != -1){
                    char c = (char) data;
                    resArray += c;
                    data = reader.read();
                }
                Log.i("Bierlijst", resArray);
                JSONArray jsonArray = new JSONArray(resArray);
                int index = 0;
                for (int i = 0; i < jsonArray.length(); i++){
                    JSONObject jsonObject;
                    jsonObject = jsonArray.getJSONObject(i);
                    if (jsonObject.has("title")){
                        bierID.add(jsonObject.getString("nid"));
                        bieren_title_list.add(jsonObject.getString("title"));
                        publishProgress("Downloaded: " + index);
                        index++;
                    }
                }

            } catch (Exception e){
                e.printStackTrace();
            } finally {
                if (connection != null){
                    connection.disconnect();
                }
                if (in != null){
                    try {
                        in.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (reader != null){
                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            String update = values[0];
            bierenDownloadView.setText(update);
            bierenDownloadView.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            bierenAdapter.notifyDataSetChanged();
            bierenDownloadView.setVisibility(View.GONE);
        }
    }

    class FestivalDownloadTask extends AsyncTask<Void, String, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            String resArray = "";
            HttpURLConnection connection = null;
            InputStream in = null;
            InputStreamReader reader = null;

            try {
                URL urlIDS = new URL(URL_Festival);
                connection = (HttpURLConnection) urlIDS.openConnection();
                in = connection.getInputStream();
                reader = new InputStreamReader(in);
                int data = reader.read();
                while (data != -1){
                    char c = (char) data;
                    resArray += c;
                    data = reader.read();
                }
                Log.i("FestivalLijst", resArray);
                JSONArray jsonArray = new JSONArray(resArray);
                int index = 0;
                for (int i = 0; i < jsonArray.length(); i++){
                    JSONObject jsonObject;
                    jsonObject = jsonArray.getJSONObject(i);
                    if (jsonObject.has("title")){
                        eventsID = jsonObject.getString("nids");
                        festival_title_list.add(jsonObject.getString("title"));
                        publishProgress("Downloaded: " + index);
                        index++;
                    }
                }

            } catch (Exception e){
                e.printStackTrace();
            } finally {
                if (connection != null){
                    connection.disconnect();
                }
                if (in != null){
                    try {
                        in.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (reader != null){
                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            new EventsDownloadTask().execute();
        }
    }

    class EventsDownloadTask extends AsyncTask<Void, String, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            String resArray = "";
            HttpURLConnection connection = null;
            InputStream in = null;
            InputStreamReader reader = null;

            String ID = eventsID.replace(", ","%2C");
            String url = URL_Events + ID;
            try {
                URL urlIDS = new URL(url);
                connection = (HttpURLConnection) urlIDS.openConnection();
                in = connection.getInputStream();
                reader = new InputStreamReader(in);
                int data = reader.read();
                while (data != -1){
                    char c = (char) data;
                    resArray += c;
                    data = reader.read();
                }
                Log.i("EventLijst", resArray);
                JSONArray jsonArray = new JSONArray(resArray);
                int index = 0;
                for (int i = 0; i < jsonArray.length(); i++){
                    JSONObject jsonObject;
                    jsonObject = jsonArray.getJSONObject(i);
                    if (jsonObject.has("title")){
                        eventID.add(jsonObject.getString("nid"));
                        evenementen_title_list.add(jsonObject.getString("title"));
                        publishProgress("Downloaded: " + index);
                        index++;
                    }
                }

            } catch (Exception e){
                e.printStackTrace();
            } finally {
                if (connection != null){
                    connection.disconnect();
                }
                if (in != null){
                    try {
                        in.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (reader != null){
                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            String update = values[0];
            evenementenDownloadView.setText(update);
            evenementenDownloadView.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            evenementenAdapter.notifyDataSetChanged();
            evenementenDownloadView.setVisibility(View.GONE);
        }
    }

    class TypesDownloadTask extends AsyncTask<Void, String, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            String resArray = "";
            HttpURLConnection connection = null;
            InputStream in = null;
            InputStreamReader reader = null;

            try {
                URL urlIDS = new URL(URL_Types);
                connection = (HttpURLConnection) urlIDS.openConnection();
                in = connection.getInputStream();
                reader = new InputStreamReader(in);
                int data = reader.read();
                while (data != -1){
                    char c = (char) data;
                    resArray += c;
                    data = reader.read();
                }
                Log.i("TypesLijst", resArray);
                JSONArray jsonArray = new JSONArray(resArray);
                int index = 0;
                for (int i = 0; i < jsonArray.length(); i++){
                    JSONObject jsonObject;
                    jsonObject = jsonArray.getJSONObject(i);
                    if (jsonObject.has("name")){
                        types_title_list.add(jsonObject.getString("name"));
                        publishProgress("Downloaded: " + index);
                        index++;
                    }
                }

            } catch (Exception e){
                e.printStackTrace();
            } finally {
                if (connection != null){
                    connection.disconnect();
                }
                if (in != null){
                    try {
                        in.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (reader != null){
                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            String update = values[0];

        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

        }
    }
}
