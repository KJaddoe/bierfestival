package com.rocdev.android.bierfestival;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class BrouwerActivity extends AppCompatActivity {
    String nid, email;
    String url = "http://app.flevobierfestival.nl/rest/brouwer/";
    String naam, beschrijving, imageUrl;
    TextView downloadView, titleView, beschrijvingView;
    ImageView imageView;
    LinearLayout line;

    // bieren list
    ListView bierLijst;
    ArrayAdapter<String> bierenAdapter;
    ArrayList<String> bieren_title_list, bieren_URL_list, bierID;
    String URL_Bieren = "http://app.flevobierfestival.nl/rest/bieren";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_brouwer);

        downloadView = (TextView) findViewById(R.id.downloadView);
        imageView = (ImageView) findViewById(R.id.imageView);

        Intent intent = getIntent();
        nid = intent.getStringExtra("brouwerID");
        email = intent.getStringExtra("email");

        titleView = (TextView) findViewById(R.id.titel);
        beschrijvingView = (TextView) findViewById(R.id.beschrijving);
        beschrijvingView.setMovementMethod(new ScrollingMovementMethod());

        //bieren list
        bierLijst = (ListView) findViewById(R.id.bierLijst);
        bieren_URL_list = new ArrayList<>();
        bieren_title_list = new ArrayList<>();
        bierID = new ArrayList<>();
        bierenAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, bieren_title_list);
        bierLijst.setAdapter(bierenAdapter);
        bierLijst.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Intent intent = new Intent(getApplicationContext(), BierActivity.class);
                intent.putExtra("bierID", bierID.get(position));
                intent.putExtra("email", email);
                startActivity(intent);
            }
        });


        downloadView.setVisibility(View.VISIBLE);

        new BrouwerDownloadTask().execute();
        new BierDownloadTask().execute();
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView imageView;

        public DownloadImageTask(ImageView imageView) {
            this.imageView = imageView;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {imageView.setImageBitmap(result);}
    }

    class BrouwerDownloadTask extends AsyncTask<Void, String, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            String resArray = "";
            HttpURLConnection connection = null;
            InputStream in = null;
            InputStreamReader reader = null;

            try {
                URL urlIDS = new URL(url + nid);
                connection = (HttpURLConnection) urlIDS.openConnection();
                in = connection.getInputStream();
                reader = new InputStreamReader(in);
                int data = reader.read();
                while (data != -1){
                    char c = (char) data;
                    resArray += c;
                    data = reader.read();
                }
                Log.i("brouwer", resArray);
                JSONArray jsonArray = new JSONArray(resArray);
                int index = 0;
                for (int i = 0; i < jsonArray.length(); i++){
                    JSONObject jsonObject;
                    jsonObject = jsonArray.getJSONObject(i);
                    if (jsonObject.has("title")){
                        naam = jsonObject.getString("title");
                        beschrijving = jsonObject.getString("body");
                        imageUrl = jsonObject.getString("field_image");
                        publishProgress("Downloaded: " + index);
                        index++;
                    }
                }

            } catch (Exception e){
                e.printStackTrace();
            } finally {
                if (connection != null){
                    connection.disconnect();
                }
                if (in != null){
                    try {
                        in.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (reader != null){
                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            String update = values[0];
            downloadView.setText(update);
            downloadView.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            downloadView.setVisibility(View.GONE);
            titleView.setText("" + naam);
            beschrijvingView.setText("" + beschrijving);
            new DownloadImageTask((ImageView) findViewById(R.id.imageView))
                    .execute(imageUrl);
        }
    }
    class BierDownloadTask extends AsyncTask<Void, String, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            String resArray = "";
            HttpURLConnection connection = null;
            InputStream in = null;
            InputStreamReader reader = null;

            try {
                URL urlIDS = new URL(URL_Bieren);
                connection = (HttpURLConnection) urlIDS.openConnection();
                in = connection.getInputStream();
                reader = new InputStreamReader(in);
                int data = reader.read();
                while (data != -1){
                    char c = (char) data;
                    resArray += c;
                    data = reader.read();
                }
                Log.i("bieren", resArray);
                JSONArray jsonArray = new JSONArray(resArray);
                int index = 0;
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject;
                    jsonObject = jsonArray.getJSONObject(i);
                    if (jsonObject.has("title")){
                        if (jsonObject.getString("field_brouwer").equals(naam)) {
                            bierID.add(jsonObject.getString("nid"));
                            bieren_title_list.add(jsonObject.getString("title"));
                            bieren_URL_list.add("http://app.flevobierfestival.nl/node/"+jsonObject.getString("nid"));
                            publishProgress("Downloaded: " + index);
                            index++;
                        }
                    }
                }

            } catch (Exception e){
                e.printStackTrace();
            } finally {
                if (connection != null){
                    connection.disconnect();
                }
                if (in != null){
                    try {
                        in.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (reader != null){
                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            String update = values[0];
            downloadView.setText(update);
            downloadView.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            bierenAdapter.notifyDataSetChanged();
            downloadView.setVisibility(View.GONE);
            line = (LinearLayout) findViewById(R.id.beschrijvingView);
            line.setVisibility(View.VISIBLE);
        }
    }
}
