package com.rocdev.android.bierfestival;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Sybren on 14-6-2016.
 */
public class RegisterRequest extends StringRequest {
    private static final String REGISTER_REQUEST_URL = "http://app.flevobierfestival.nl/Register.php";
    private Map<String, String> params;

    public RegisterRequest(String voornaam, String achternaam, String email, String password, String repassword, Response.Listener<String> listener) {
        super(Method.POST, REGISTER_REQUEST_URL, listener, null);
        params = new HashMap<>();
        params.put("voornaam", voornaam);
        params.put("achternaam", achternaam);
        params.put("email", email);
        params.put("password", password);
        params.put("repassword", repassword);
    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }
}